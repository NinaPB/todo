from django.db import models

# Create your models here.
from django.db import models
from django.contrib import admin

#https://code.tutsplus.com/articles/intro-to-django-building-a-to-do-list--net-2871

class Category(models.Model):
    name = models.CharField(max_length=100)
    
class Todo(models.Model): #Table name, has to wrap models.Model to get the functionality of Django.
    status_choices = [
        ("D", "Done"),
        ("T", "Todo"),
        ("C", "Cancelled"),
    ]
         
    name = models.CharField(max_length=100, unique=True)
    description = models.TextField() #Like a TEXT field
    deadline = models.DateTimeField(null=True)
    status = models.CharField(max_length=1, choices=status_choices, default="T")
    category = models.ForeignKey(Category, on_delete=models.CASCADE, null=True)
    created = models.DateTimeField() #Like a DATETIME field
    def __str__(self):
        return f"{self.name} {self.category} {self.status} {self.deadline}"
    
    def __unicode__(self): #Tell it to return as a unicode string (The name of the to-do item) rather than just Object.
        return self.name
        
