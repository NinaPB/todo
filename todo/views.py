from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from todo.models import Todo
from django.shortcuts import render

def index(request):
    #return HttpResponse("Hello, world. You're at the todo index.")
    items = todo.objects.all() #ORM queries the database for all of the to-do entries.
    return render(request,'todo/index.html', {'items': items}) #Responds with passing the object items (contains info from the DB) to the template index.html



def detail(request, todo_id):
  # do something with the question_id
  todo = get_object_or_404(Todo, pk=todo_id)
  return render(request, "todo/detail.html", {'todo' : todo})

def overview(request):
    todos = Todo.objects.all()
    return render(request, 'todo/overview.html', {"todos" : todos})
    
def status_change(request, todo_id):
    todo = get_object_or_404(Todo, pk=todo_id)
    todo.status = request.POST["status"] # selber name wie im <select>-tag
    todo.save()
    return HttpResponseRedirect(reverse('todo:overview'))
    
def new(request):
    name = request.POST["name"]
    description = request.POST["description"]
    status = request.POST["status"]
    deadline = request.POST["deadline"]
    category = Category.objects.get(pk=request.POST["category"])

    print(name, description, status, deadline, category)

    Todo.objects.create(name=name,
                        description=description,
                        status=status,
                        deadline=deadline,
                        category=category)
    return HttpResponseRedirect(reverse('todo:overview'))