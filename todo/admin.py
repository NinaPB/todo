#from django.contrib import admin

# Register your models here.
from django.contrib import admin
# Im Tutorial werden hier beide Klassen direkt importiert.
# Ich finde es so übersichtlicher und muss nicht jede Klasse
# an zwei Stellen eintragen.
from . import models
from .models import Category, Todo
#admin.site.register(Category)


class TodoInLine(admin.TabularInline):
    model = Todo
    
class CategoryAdmin(admin.ModelAdmin):
    inlines = [TodoInLine]
    
admin.site.register(Category, CategoryAdmin)
admin.site.register(Todo)